package com.nikedlab.roller.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.nikedlab.roller.LogRoller
import com.nikedlab.roller.lib.LogRollerSettings

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        applicationContext.externalCacheDir?.absolutePath?.let {
            val logFile = "$it/logs/log.txt"
            val settings = LogRollerSettings.Builder().setLogFilePath(logFile).setMaxFileSize(102400)
            LogRoller.setup(settings)
        }

    }

    override fun onResume() {
        super.onResume()

        (1..9).forEach {
            LogRoller.i("LogRoller -> info =====100%======== %d =============== %s", it, it.toString(2))
            LogRoller.i(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))
            LogRoller.d(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))
            LogRoller.e(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))
            LogRoller.w(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))
            LogRoller.ass(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))
            LogRoller.wtf(this.javaClass, "LogRoller -> info ============= %d =============== %s", it, it.toString(2))



            Log.i("", "2e q qw qwe wq qw e ")
        }


    }
}