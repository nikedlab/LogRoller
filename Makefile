dist: clean assemble publishToMaven

clean:
	@./gradlew clean

assemble:
	@./gradlew --parallel roller:assemble

publishToMaven:
	./gradlew roller:publish