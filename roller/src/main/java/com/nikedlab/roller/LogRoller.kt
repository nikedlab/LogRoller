package com.nikedlab.roller

import com.nikedlab.roller.lib.LogRollerSettings
import com.nikedlab.roller.lib.LogRollerWorker
import org.jetbrains.annotations.Nullable

object LogRoller {

    private val logRollerWorker by lazy {
        LogRollerWorker()
    }

    fun setup() = apply { logRollerWorker.setup() }

    fun setup(settings: LogRollerSettings): LogRoller = apply { logRollerWorker.setup(settings) }

    fun setup(settings: LogRollerSettings.Builder): LogRoller = apply { logRollerWorker.setup(settings.build())  }

    fun ass(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.ass(message, *objs)
    fun e(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.e(message, *objs)
    fun d(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.d(message, *objs)
    fun w(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.w(message, *objs)
    fun i(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.i(message, *objs)
    fun wtf(@Nullable message: String?, vararg objs: Any?) = logRollerWorker.wtf(message, *objs)

    fun   i(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) =   logRollerWorker.i("${clazz.simpleName}:-> $message", *objs)
    fun   e(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) =   logRollerWorker.e("${clazz.simpleName}:-> $message", *objs)
    fun   d(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) =   logRollerWorker.d("${clazz.simpleName}:-> $message", *objs)
    fun   w(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) =   logRollerWorker.w("${clazz.simpleName}:-> $message", *objs)
    fun ass(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) = logRollerWorker.ass("${clazz.simpleName}:-> $message", *objs)
    fun wtf(clazz: Class<*>, @Nullable message: String?, vararg objs: Any?) = logRollerWorker.wtf("${clazz.simpleName}:-> $message", *objs)

}