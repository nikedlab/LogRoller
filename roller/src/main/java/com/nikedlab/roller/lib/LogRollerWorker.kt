package com.nikedlab.roller.lib

import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter
import java.util.zip.GZIPOutputStream

@Suppress("MemberVisibilityCanBePrivate", "unused")
class LogRollerWorker {

    private val logger = Logger.getLogger("LogRoller.logger")

    private lateinit var settings: LogRollerSettings
    private var logFileAttributes: LogFileAttributes? = null

    fun setup(s: LogRollerSettings? = null) {
        val handler = ConsoleHandler()
        handler.level = Level.ALL
        handler.formatter = SimpleFormatter()
        logger.addHandler(handler)

        settings = s ?: LogRollerSettings.Builder().build()
        settings.logFilePath?.let {
            logFileAttributes = LogFileAttributes(it)
            logFileAttributes?.logFolderPath?.let { logFolderPath ->
                prepareFolder(logFolderPath)
            }
        }
    }

    private fun prepareFolder(folder: String): Boolean {
        val tmpDir = File(folder)
        if (!tmpDir.exists()) {
            if (tmpDir.mkdir()) {
                return true
            }
        } else {
            return true
        }
        return false
    }

    fun ass(message: String?, vararg args: Any?) {
        val msg = Formatter.format("$message", *args)
        writeLogToFile(msg, "ERROR", isAss = true)
    }

    fun e(message: String?, vararg args: Any?) {
        val msg = Formatter.format("$message", *args)
        logger.severe("$message")
        writeLogToFile(msg, "ERROR")
    }

    fun d(message: String?, vararg args: Any?) {
        val msg = Formatter.format("$message", *args)
        logger.config("$message")
        writeLogToFile(msg, "DEBUG")
    }

    fun w(message: String?, vararg args: Any?) {
        val msg = Formatter.format("$message", *args)
        logger.warning("$message")
        writeLogToFile(msg, "WARNING")
    }

    fun i(message: String?, vararg objs: Any?) {
        val msg = Formatter.format("$message", *objs)
        logger.info("$message")
        writeLogToFile(msg, "INFO")
    }

    fun wtf(message: String?, vararg args: Any?) {
        val msg = Formatter.format("$message", *args)
        logger.severe("$message")
        writeLogToFile(msg, "WTF")
    }

    private fun printLogToLogcat(message: String? = "Error", type: String) {
        when (type) {
            "i" ->  logger.info(message)
            "w" ->  logger.warning(message)
            "d" ->  logger.config(message)
            "e" ->  logger.severe(message)
            else -> logger.severe(message)
        }
    }

    private fun writeLogToFile(message: String?, type: String, isAss: Boolean = false) {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH)
        val date = Date()
        val time = dateFormat.format(date)
        try {
            writeLog("$type($time): $message")
        } catch (e: Exception) {
            logger.severe("${e.message}")
        }
    }

    private fun writeLog(message: String?) {
        if (getFileSize() < settings.fileSize) {
            writeLineToFile("$message")
        } else {
            backupFile()
        }
    }

    private fun backupFile() {
        settings.logFilePath?.let {
            val file = File(it)
            reverseFiles()
            val newFile = File(settings.logFilePath + ".1")
            file.renameTo(newFile)
            file.delete()
        }
    }

    private fun compressGzipFile(file: String, gzipFile: String) {
        try {
            val fis = FileInputStream(file)
            val fos = FileOutputStream(gzipFile)
            val gzipOS = GZIPOutputStream(fos)
            val buffer = ByteArray(1024)
            var len: Int
            while (fis.read(buffer).also { len = it } != -1) {
                gzipOS.write(buffer, 0, len)
            }
            //close resources
            gzipOS.close()
            fos.close()
            fis.close()
        } catch (e: IOException) {
            logger.severe("${e.message}")
        }
    }

    /**
     * File writer
     * @param line [java.lang.String] for writing
     * @return Writing result
     */
    private fun writeLineToFile(line: String): Boolean {
        settings.logFilePath?.let {
            val file = File(it)
            try {
                if (!file.exists()) {
                    file.createNewFile()
                }
                if (file.canWrite()) {
                    val bw =
                        BufferedWriter(FileWriter(file, true))
                    bw.append(line)
                    bw.newLine()
                    bw.close()
                } else {
                    return false
                }
            } catch (e: IOException) {
                logger.severe("${e.message}")
                return false
            }
            return true
        }
        return false
    }

    /**
     * Rename log files
     */
    private fun reverseFiles() {
        val files = getFilesList()

        if (files.isNotEmpty()) {
            for (origFile in files) {
                val segments = origFile.name.split("\\.".toRegex()).toTypedArray()
                var segment = segments[segments.size - 1]
                var flag = false
                if ("gz" == segment) {
                    segment = segments[segments.size - 2]
                    flag = true
                }
                try {
                    origFile.parent?.let {
                        val index = try {
                            segment.toInt() + 1
                        } catch (e: java.lang.NumberFormatException) {
                            1
                        }

                        if (flag) {
                            origFile.renameTo(File(it + File.separator + logFileAttributes?.fileName + "." + index + ".gz.bck"))
                        } else {
                            val name = it + File.separator + logFileAttributes?.fileName + "." + index
                            val dest = File("$name.bck")
                            origFile.renameTo(dest)
                            compressGzipFile(dest.absolutePath, "$name.gz.bck")
                            dest.delete()
                        }
                    }
                } catch (e: NumberFormatException) {
                    logger.severe("${e.message}")
                    removeBck()
                }
            }
            backRenaming()
        }
    }

    /**
     * Remove all backup files
     */
    private fun removeBck() {
        val files = getFilesList()
        if (files.isNotEmpty()) {
            for (file in files) {
                if (file.name.endsWith(".bck")) {
                    file.delete()
                }
            }
        }
    }

    /**
     * Rename log files
     */
    private fun backRenaming() {
        val files = getFilesList()
        for (file in files) {
            val fileName = file.absolutePath.replace(".bck", "")
            val newFile = File(fileName)
            file.renameTo(newFile)
            if (newFile.name == logFileAttributes?.fileName + "." + (settings.maxLogFiles + 1) + ".gz") {
                newFile.delete()
            }
        }
    }

    /**
     * Getter for file list in log folder
     * @return File list
     */
    private fun getFilesList(): List<File> {
        logFileAttributes?.logFolderPath?.also { attr ->
            val file = File(attr)
            file.let { folder ->
                val arr = folder.listFiles { _, name -> name.startsWith("${logFileAttributes?.fileName}.") } ?: emptyArray()
                return arr.toList()
            }
        }
        return emptyList()
    }

    /**
     * File size getter
     * @return Size
     */
    private fun getFileSize(): Long {
        settings.logFilePath?.let {
            val file = File(it)
            return file.length()
        }
        return 0
    }

    private class LogFileAttributes(logFile: String) {
        val fileName: String
        val filePath: String
        val logFolderPath: String?

        init {
            val log = File(logFile)
            fileName = log.name
            filePath = logFile
            logFolderPath = log.parent ?: null
        }
    }

}