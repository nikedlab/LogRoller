package com.nikedlab.roller.lib

class LogRollerSettings private constructor() {
    var fileSize: Long = 1024000
    var maxLogFiles = 30
    var logFilePath: String? = null

    data class Builder(
        var fileSize: Long = 1024000,
        var maxLogFiles: Int = 30,
        var logFilePath: String? = null
    ) {

        fun setMaxFileSize(fileSize: Long) = apply { this.fileSize = fileSize }
        fun setMaxLogsFiles(maxLogFiles: Int) = apply { this.maxLogFiles = maxLogFiles }
        fun setLogFilePath(logFilePath: String) = apply { this.logFilePath = logFilePath }

        fun build(): LogRollerSettings {
            val settings = LogRollerSettings()
            settings.fileSize = this.fileSize
            settings.logFilePath = this.logFilePath
            settings.maxLogFiles = this.maxLogFiles
            return settings
        }

    }

}