package com.nikedlab.roller.lib;

public class Formatter {

    static String format(String message, Object... args) {
        String newLine = message
                .replaceAll("%", "%%")
                .replaceAll("%%s", "%s")
                .replaceAll("%%d", "%d");
        return String.format(newLine, args);
    }

}
